<?php

/**
 * Initialize the options before anything else. 
 */
add_action('admin_init', 'custom_theme_options', 1);

/**
 * Build the custom settings & update OptionTree.
 */
function custom_theme_options() {
    /**
     * Get a copy of the saved settings array. 
     */
    $saved_settings = get_option('option_tree_settings', array());

    /**
     * Custom settings array that will eventually be 
     * passes to the OptionTree Settings API Class.
     */
    $custom_settings = array(
        'contextual_help' => array(
            'content' => array(
                array(
                    'id' => 'general_help',
                    'title' => 'General1',
                    'content' => '<p>Help content goes here!</p>'
                )
            ),
            'sidebar' => '<p>Sidebar content goes here!</p>',
        ),
        'sections' => array(
            array(
                'id' => 'header',
                'title' => 'Header Settings'
            ),
            array(
                'id' => 'footersetting',
                'title' => 'Footer Settings'
            ),
        ),
        'settings' => array(
            array(
                'id' => 'logo_image',
                'label' => 'Logo',
                'desc' => '',
                'std' => '',
                'section' => 'header',
                'type' => 'upload',
                'class' => '',
                'choices' => array()
            ),
           
            array(
                'id' => 'copyright',
                'label' => 'Copyright',
                'desc' => 'Enter Text Here.',
                'std' => '',
                'section' => 'footersetting',
                'type' => 'text',
                'class' => '',
                'choices' => array()
            ),
            array(
                'id' => 'brand',
                'label' => 'Brand',
                'desc' => 'Enter Text Here.',
                'std' => '',
                'section' => 'footersetting',
                'type' => 'text',
                'class' => '',
                'choices' => array()
            ),
            array(
                'id' => 'design_by',
                'label' => 'Design By',
                'desc' => 'Enter Text Here.',
                'std' => '',
                'section' => 'footersetting',
                'type' => 'text',
                'class' => '',
                'choices' => array()
            ),
            array(
                'id' => 'facebook',
                'label' => 'facebook Url',
                'desc' => 'Enter Url Here.',
                'std' => '',
                'section' => 'footersetting',
                'type' => 'text',
                'class' => '',
                'choices' => array()
            ),
            array(
                'id' => 'twitter',
                'label' => 'Twitter Url',
                'desc' => 'Enter Url Here.',
                'std' => '',
                'section' => 'footersetting',
                'type' => 'text',
                'class' => '',
                'choices' => array()
            ),
            array(
                'id' => 'rss',
                'label' => 'Rss Url',
                'desc' => 'Enter Url Here.',
                'std' => '',
                'section' => 'footersetting',
                'type' => 'text',
                'class' => '',
                'choices' => array()
            ),
             array(
                'id' => 'background_bg',
                'label' => 'Background Image',
                'desc' => 'Enter Text Here.',
                'std' => '',
                'section' => 'footersetting',
                'type' => 'upload',
                'class' => '',
                'choices' => array()
            ),
            array(
                'id' => 'contact_us',
                'label' => 'Bottom Footer Heading',
                'desc' => 'Enter Text Here.',
                'std' => '',
                'section' => 'footersetting',
                'type' => 'text',
                'class' => '',
                'choices' => array()
            ),
            array(
                'id' => 'email',
                'label' => 'Email For Diplay',
                'desc' => 'Enter Email Here.',
                'std' => '',
                'section' => 'footersetting',
                'type' => 'text',
                'class' => '',
                'choices' => array()
            ),
            array(
                'id' => 'email_address',
                'label' => 'Email Address',
                'desc' => 'Enter Email Here.',
                'std' => '',
                'section' => 'footersetting',
                'type' => 'text',
                'class' => '',
                'choices' => array()
            ),
            array(
                'id' => 'address',
                'label' => 'Address',
                'desc' => 'Enter Address Here.',
                'std' => '',
                'section' => 'footersetting',
                'type' => 'text',
                'class' => '',
                'choices' => array()
            ),
            
        )
    );

    /* settings are not the same update the DB */
    if ($saved_settings !== $custom_settings) {
        update_option('option_tree_settings', $custom_settings);
    }
}

?>