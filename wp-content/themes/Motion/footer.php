<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

            <section id="footer">
                <div class="container">
                    <div class="row d-flex">
                        <div class="copyright mr-auto p-2 align-self-center">
                            <p><?php echo get_option_tree('copyright','',false); ?><span><?php echo get_option_tree('brand','',false); ?></span><?php echo get_option_tree('design_by','',false); ?></p>
                        </div>
                        <div class="social ml-auto p-2">
                            <ul>
                                <li><a href="<?php echo get_option_tree('facebook','',false); ?>" target="blank"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="<?php echo get_option_tree('twitter','',false); ?>" target="blank"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="<?php echo get_option_tree('rss','',false); ?>" target="blank"><i class="fas fa-rss"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <!--bottom footer-->
            
            <section id="bottom-footer" style="background-image: url('<?php echo get_option_tree('background_bg'); ?>');">
                <div class="container">
                    <div class="contact-us">
                        <span><?php echo get_option_tree('contact_us'); ?></span>
                        <a href="<?php echo get_option_tree('email_address'); ?>"><?php echo get_option_tree('email'); ?></a>
                        <p><?php echo get_option_tree('address'); ?></p>
                    </div>
                </div>
            </section>
            
        </div>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

<!--        <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>-->
        <script src="<?php bloginfo('template_directory')?>/assets/js/jquery-1.7.min.js" type="text/javascript" charset="utf-8"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.js"></script>
        <script src="<?php bloginfo('template_directory')?>/assets/js/jquery.featureCarousel.min.js" type="text/javascript" charset="utf-8"></script>
        <script src="https://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                var carousel = $("#carousel").featureCarousel({
                    // include options like this:
                    // (use quotes only for string values, and no trailing comma after last option)
                    // option: value,
                    // option: value
                });

                $("#but_prev").click(function () {
                    carousel.prev();
                });
                $("#but_pause").click(function () {
                    carousel.pause();
                });
                $("#but_start").click(function () {
                    carousel.start();
                });
                $("#but_next").click(function () {
                    carousel.next();
                });
                $('.responsive').slick({
                    dots: true,
                    infinite: false,
                    speed: 300,
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    responsive: [
                        {
                            breakpoint: 1024,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 3,
                                infinite: true,
                                dots: true
                            }
                        },
                        {
                            breakpoint: 600,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 2
                            }
                        },
                        {
                            breakpoint: 480,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1
                            }
                        }
                        // You can unslick at a given breakpoint now by adding:
                        // settings: "unslick"
                        // instead of a settings object
                    ]
                });
                $('.single-item').slick();
            });

        </script>
    </body>
</html>

<?php wp_footer(); ?>

</body>
</html>
