<?php
/* Template Name: Form */

get_header();
?>

<div class="container">
    <div class="row justify-content-md-center align-self-center">
        <div class="col-sm-12 col-md-8 col-lg-8">


            <form id="custom-form" action="<?php echo site_url(); ?>/form" method="post">

                <div class="form-group row">
                    <label class="col-sm-4 col-form-label custom-label" for="collection">Item Choice</label>
                    <div class="col-sm-8">
                        <select class="form-control" name="collection" id="collection">
                            <?php
                            $get_term = get_terms(
                                    array(
                                        'post_types' => 'product',
                                        'taxonomy' => 'collection_detail'
                                    )
                            );
//                            print_r( $get_term);
                            foreach ($get_term as $term) {
                                ?><option>
                                    <?php echo $term->name; ?>
                                </option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-4 col-form-label custom-label" for="size">Size *</label>
                    <div class="col-sm-8">
                        <select class="form-control" name="size" id="size">
                            <?php
                            $size_detail = get_terms(
                                    array(
                                        'post_types' => 'product',
                                        'taxonomy' => 'size_detail'
                                    )
                            );
//                            print_r( $get_term);
                            foreach ($size_detail as $size) {
                                ?>
                            <option><?php echo $size->name;?></option>
                                <?php
                            }
                            ?>

                        </select>
                    </div>
                </div>

                <div id="name-group" class="form-group row">
                    <label class="col-sm-4 col-form-label custom-label">wholesale Price *</label>
                    <div class="col-sm-8">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">&dollar;</div>
                            </div>
                            <input type="text" name="wholesale-price" class="form-control" id="wholesale-price">
                        </div>
                    </div>
                </div>

                <div id="email-group" class="form-group row">
                    <label for="inputPassword" class="col-sm-4 col-form-label custom-label">Price *</label>
                    <div class="col-sm-8">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">&dollar;</div>
                            </div>
                            <input type="text" name="price" class="form-control" id="Price">
                        </div>
                    </div>
                </div>

                <div id="superhero-group" class="form-group row">
                    <label class="col-sm-4 custom-label">Description *</label>
                    <div class="col-sm-8">
                        <textarea class="form-control" name="description" id="Description" rows="3"></textarea>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-4 custom-label">Upload product images *</label>
                    <div class="col-sm-8">
                        <input type="file" id="files" name="files" multiple />
                        <output id="list"></output>
                    </div>

                </div>

                <div class="form-group row">
                    <label class="col-sm-4 col-form-label custom-label">Purchase date *</label>
                    <div class="col-sm-8">
                        <input type="date" name="date" class="form-control" id="purchase-date">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-4 col-form-label custom-label">Quantity received *</label>
                    <div class="col-sm-8">
                        <input type="text" name="quantity" class="form-control" id="recevied-quantity">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-4 col-form-label custom-label" for="question">Customize Claim Form *</label>
                    <div class="col-sm-8">
                        <select class="form-control" name="add_questions" id="question">
                            <?php
                            $claim_form = get_terms(
                                    array(
                                        'post_types' => 'product',
                                        'taxonomy' => 'claim_form_detail'
                                    )
                            );
//                            print_r( $get_term);
                            foreach ($claim_form as $claim) {
                                ?>
                            <option><?php echo $claim->name; ?></option>
                             <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-4"></div>
                    <div class="co-sm-8 col align-self-end">
                        <p>Don'tsee what you are looking for?<br>press this button to add new claim</p>
                        <button type="button" class="btn question-button">Add new question</button>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-4"></div>
                    <div class="co-sm-8 col align-self-end">
                        <input type="submit" name="submit" value="submit" />
                        <!--                        <button type="submit" name="submit" id="submit_data" class="btn submit">Submit</button>
                                                <button type="submit" class="btn submit-add">submit and add another</button>-->
                    </div>
                </div>

            </form>
        </div>
    </div>

</div>

<?php
if (isset($_POST['submit'])) {
    $table = wp_custom_product;

    $data = array(
        'collection' => $_POST['collection'],
        'size' => $_POST['size'],
        'wholesale-price' => $_POST['wholesale-price'],
        'price' => $_POST['price'],
        'description' => $_POST['description'],
        'date' => $_POST['date'],
        'quantity' => $_POST['quantity'],
        'add_questions' => $_POST['add_questions'],
    );
    $success = $wpdb->insert($table, $data);
    if ($success) {
        echo 'Data has been Saved';
    }
}
get_footer();


