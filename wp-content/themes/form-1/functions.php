<?php

function product_cpt() {
    $labels = array(
        'name' => _x('product', 'post type general name'),
        'singular_name' => _x('product', 'post type singular name'),
        'add_new' => _x('Add New', 'product'),
        'add_new_item' => __('Add New product'),
        'edit_item' => __('Edit product'),
        'new_item' => __('New product'),
        'all_items' => __('All product'),
        'view_item' => __('View product'),
        'search_items' => __('Search product'),
        'not_found' => __('No product found'),
        'not_found_in_trash' => __('No product found in the Trash'),
        'parent_item_colon' => '',
        'menu_name' => 'product'
    );
    $args = array(
        'labels' => $labels,
        'description' => 'product',
        'public' => true,
        'menu_icon' => 'dashicons-products',
        'menu_position' => 30,
        'supports' => array('title',),
        'has_archive' => true,
    );
    register_post_type('product', $args);
}

add_action('init', 'product_cpt');


add_action('init', 'collection_taxonomies');

function collection_taxonomies() {
    $labels = array(
        'name' => _x('Collection', 'taxonomy general name', 'textdomain'),
        'singular_name' => _x('Collection', 'taxonomy singular name', 'textdomain'),
        'search_items' => __('Search Collection', 'textdomain'),
        'all_items' => __('All Collection', 'textdomain'),
        'parent_item' => __('Parent Collection', 'textdomain'),
        'parent_item_colon' => __('Parent Collection:', 'textdomain'),
        'edit_item' => __('Edit Collection', 'textdomain'),
        'update_item' => __('Update Collection', 'textdomain'),
        'add_new_item' => __('Add New Collection', 'textdomain'),
        'new_item_name' => __('New Collection Name', 'textdomain'),
        'menu_name' => __('Collection', 'textdomain'),
    );

    $collection = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'collection_detail'),
    );

    register_taxonomy('collection_detail', array('product'), $collection);
}

//texonomie for size


add_action('init', 'size_taxonomies');

function size_taxonomies() {
    $labels = array(
        'name' => _x('Size', 'taxonomy general name', 'textdomain'),
        'singular_name' => _x('Size', 'taxonomy singular name', 'textdomain'),
        'search_items' => __('Search Size', 'textdomain'),
        'all_items' => __('All Size', 'textdomain'),
        'parent_item' => __('Parent Size', 'textdomain'),
        'parent_item_colon' => __('Parent Size:', 'textdomain'),
        'edit_item' => __('Edit Size', 'textdomain'),
        'update_item' => __('Update Size', 'textdomain'),
        'add_new_item' => __('Add New Size', 'textdomain'),
        'new_item_name' => __('New Size Name', 'textdomain'),
        'menu_name' => __('Size', 'textdomain'),
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'size_detail'),
    );

    register_taxonomy('size_detail', array('product'), $args);
}

//claim question texonomie


add_action('init', 'claim_form_taxonomies');

function claim_form_taxonomies() {
    $labels = array(
        'name' => _x('Claim Form', 'taxonomy general name', 'textdomain'),
        'singular_name' => _x('Claim Form', 'taxonomy singular name', 'textdomain'),
        'search_items' => __('Search Claim Form', 'textdomain'),
        'all_items' => __('All Claim Form', 'textdomain'),
        'parent_item' => __('Parent Claim Form', 'textdomain'),
        'parent_item_colon' => __('Parent Claim Form:', 'textdomain'),
        'edit_item' => __('Edit Claim Form', 'textdomain'),
        'update_item' => __('Update Claim Form', 'textdomain'),
        'add_new_item' => __('Add New Claim Form', 'textdomain'),
        'new_item_name' => __('New Claim Form Name', 'textdomain'),
        'menu_name' => __('Claim Form', 'textdomain'),
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'claim_form_detail'),
    );

    register_taxonomy('claim_form_detail', array('product'), $args);
}


