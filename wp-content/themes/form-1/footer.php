
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>


<script type="text/javascript">
  

//    $(function () {
//        $('#submit-button').click(function () {
//          var collections = $('#collection').val();
//          var sizes = $('#size').val();
//          var wholesale_prices = $('#wholesale_price').val();
//          var prices = $('#price').val();
//          var filesupload = $('#files').val();
//          var purchase_dates = $('#purchase_date').val();
//          var recevied_quantites = $('#recevied_quantity').val();
//          var questions = $('#question').val();
//          console.log('starting ajax');
//          $.ajax({
//            url: "./process.php",
//            type: "post",
//            data: { collection: collections, size: sizes, wholesale_price: wholesale_prices, price: prices, files: filesupload,
//                purchase_date: purchase_dates, recevied_quantity: recevied_quantites, question: questions
//            },
//            success: function (data) {
//              var dataParsed = JSON.parse(data);
//              console.log(dataParsed);
//            }
//          });
//
//        });
//      });


    function handleFileSelect(evt) {
        var files = evt.target.files; // FileList object

        // Loop through the FileList and render image files as thumbnails.
        for (var i = 0, f; f = files[i]; i++) {

            // Only process image files.
            if (!f.type.match('image.*')) {
                continue;
            }

            var reader = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function (theFile) {
                return function (e) {
                    // Render thumbnail.
                    var span = document.createElement('span');
                    span.innerHTML = ['<img class="thumb" src="', e.target.result,
                        '" title="', escape(theFile.name), '"/>'].join('');
                    document.getElementById('list').insertBefore(span, null);
                };
            })(f);

            // Read in the image file as a data URL.
            reader.readAsDataURL(f);
        }
    }

    document.getElementById('files').addEventListener('change', handleFileSelect, false);
</script>
<?php wp_footer(); ?>
</body>
</html>
