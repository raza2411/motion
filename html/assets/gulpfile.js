//plugin in 

var gulp = require('gulp');

var uglyfy = require('gulp-uglyfly');

var cancat_js = require('gulp-concat');

var minify_css = require('gulp-mini-css');

var sass = require('gulp-sass');


//task 
gulp.task('compile-css', function(){
	return gulp.src('src/*.css')
	 		.pipe(minify_css({ext:'-min.css'}))
			.pipe(gulp.dest('dist/css'));
});

gulp.task('concatination', function(){
	return gulp.src('src/js/*.js')
			.pipe(cancat_js('merge.js'))
			.pipe(uglyfy())
			.pipe(gulp.dest('dist/js'));
});

gulp .task('scss-compile', function(){
	return gulp.src('src/sass/*.scss')
			.pipe(sass())
			.pipe(gulp.dest('dist/css'));
});

gulp.task('watcher', function(){
	gulp.watch('src/js/*.js',['concatination']);
	gulp.watch('src/*.css',['compile-css']);
	gulp.watch('src/sass/*.scss',['scss-compile'])
});

gulp.task('default', ['watcher']);


