var tl = new TimelineLite();

tl.from("#creative h4", 2, {left:100, autoAlpha:0})
    .from("#creative span", 0.5, {right:100, autoAlpha:0},0.2)
    .staggerFrom(".navbar-nav", 0.5, {right:100, autoAlpha:0},0.2);